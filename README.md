﻿# GrupoClarin

Escriba un método Java que comprima los caracteres repetidos en una cadena dada.
La cadena resultante debería estar compuesta por #caracter1#caracter2...#caractern, donde # es la cantidad de apariciones consecutivas del caracter. 
Ejemplos:

	input1:  aaalkjjjjaqqqqqqqqqcjjjjjbbbbbbbbbadaaaoqqiuuuuuuuuuu
	output1: 3a1l1k4j1a9q1c5j9b3a1o2q1i10u
	

	input2:  bvvuuuuuuurrrrtttttaqassssssssjjjjjjkkkkkkkllllpa
	output2: 1b2v7u4r5t1a1q1a8s6j7k4l1p1a

Observación: Al parecer el primer ejemplo proporcionado cuenta con errores, puesto que faltan caracteres que no están siendo comprimidos

